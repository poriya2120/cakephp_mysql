<?php
if (!isset($channel)) :
    $channel = [];
endif;
if (!isset($channel['title'])) :
    $channel['title'] = $this->fetch('title');
endif;
<ul>
<li>
<?= $this->html->link('logout',['controller'=>'users','action'=>'logout']); ?>
</li>
</ul>
echo $this->Rss->document(
    $this->Rss->channel([], $channel, $this->fetch('content'))
);
